import React from "react";
import Header from "../components/Header";

const SubmitRecipePage = () => {
	return <>
		<Header/>
		<div className="submit">
			<div className="title">
				<div className="container">
					<div className="row">
						<div className="col-lg-12">
							<h2>Submit Recipe</h2>
						</div>
					</div>
				</div>
			</div>
			<div className="content">
				<div className="container">
					<div className="row">
						<div className="col-lg-12">

							<div className="form-group">
								<label>Recipe Title</label>
								<input type="text" className="form-control"/>
							</div>

							<div className="form-group">
								<label>Choose category</label>
								<select className="js-search-category form-control select2-hidden-accessible"
										name="category" data-placeholder="Choose Category" tabIndex={-1}
										aria-hidden="true">
									<option value="1">All</option>
									<option value="2">Breakfast</option>
									<option value="3">Lunch</option>
									<option value="4">Beverages</option>
									<option value="5">Appetizers</option>
									<option value="6">Soups</option>
									<option value="7">Salads</option>
									<option value="8">Beef</option>
									<option value="9">Poultry</option>
									<option value="10">Pork</option>
									<option value="11">Seafood</option>
									<option value="12">Vegetarian</option>
									<option value="13">Vegetables</option>
									<option value="14">Desserts</option>
									<option value="15">Canning / Freezing</option>
									<option value="16">Breads</option>
									<option value="17">Holidays</option>
								</select><span className="select2 select2-container select2-container--default"
											   dir="ltr" style={{"width": "1110px"}}><span className="selection"><span
								className="select2-selection select2-selection--single" role="combobox"
								aria-haspopup="true" aria-expanded="false" tabIndex={0}
								aria-labelledby="select2-category-86-container"><span
								className="select2-selection__rendered" id="select2-category-86-container"
								title="All">All</span><span className="select2-selection__arrow" role="presentation"><b
								role="presentation"/></span></span></span><span className="dropdown-wrapper"
																				   aria-hidden="true"/></span>
							</div>

							<div className="form-group">
								<label>Short summary</label>
								<textarea className="form-control" rows={4} required={true}/>
							</div>

							<div className="form-group">
								<label>Tag</label>
								<input type="text" className="form-control"/>
							</div>

							<div className="form-group">
								<label>Upload your photos</label>
								<input type="file" className="form-control-file"/>
							</div>

							<div className="form-group">
								<label>Ingredients:</label>

								<div id="sortable" className="ui-sortable">
									<div className="box ui-sortable-handle">
										<div className="row">
											<div className="col-lg-1 col-sm-1">
												<i className="fa fa-arrows" aria-hidden="true"/>
											</div>
											<div className="col-lg-5 col-sm-5">
												<input type="text" className="form-control"
													   placeholder="Name of ingredient"/>
											</div>
											<div className="col-lg-5 col-sm-5">
												<input type="text" className="form-control"
													   placeholder="Notes (quantity or additional info)"/>
											</div>
											<div className="col-lg-1 col-sm-1">
												<i className="fa fa-times-circle-o minusbtn" aria-hidden="true"/>
											</div>
										</div>
									</div>

								</div>
								<a href="#" className="btn btn-light">Add new ingredient</a>

							</div>

							<div className="form-group">
								<label>Directions:</label>
								<textarea className="form-control" rows={4} required={true} />
							</div>

							<div className="form-group">
								<label>Additional Informations</label>
								<hr/>
							</div>
							<div className="form-group row">
								<label className="col-sm-2 col-form-label">Yield</label>
								<div className="col-sm-10">
									<input type="text" className="form-control"/>
								</div>
							</div>
							<div className="form-group row">
								<label className="col-sm-2 col-form-label">Preparation Time</label>
								<div className="col-sm-10">
									<input type="text" className="form-control"/>
								</div>
							</div>
							<div className="form-group row">
								<label className="col-sm-2 col-form-label">Cooking Time</label>
								<div className="col-sm-10">
									<input type="text" className="form-control"/>
								</div>
							</div>

							<div className="form-group">
								<label>Nutrition Facts</label>
								<hr/>
							</div>
							<div className="form-group row">
								<label className="col-sm-2 col-form-label">Calories</label>
								<div className="col-sm-10">
									<input type="text" className="form-control"/>
								</div>
							</div>
							<div className="form-group row">
								<label className="col-sm-2 col-form-label">Total Carbohydrate</label>
								<div className="col-sm-10">
									<input type="text" className="form-control"/>
								</div>
							</div>
							<div className="form-group row">
								<label className="col-sm-2 col-form-label">Total Fat</label>
								<div className="col-sm-10">
									<input type="text" className="form-control"/>
								</div>
							</div>
							<div className="form-group row">
								<label className="col-sm-2 col-form-label">Protein</label>
								<div className="col-sm-10">
									<input type="text" className="form-control"/>
								</div>
							</div>
							<div className="form-group row">
								<label className="col-sm-2 col-form-label">Cholesterol</label>
								<div className="col-sm-10">
									<input type="text" className="form-control"/>
								</div>
							</div>

							<button type="submit" className="btn btn-submit">Submit Recipe</button>

						</div>
					</div>
				</div>
			</div>
		</div>
		</>
}

export default SubmitRecipePage;
