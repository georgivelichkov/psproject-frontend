import React from 'react';
import {
	BrowserRouter as Router,
	Route
} from "react-router-dom";
import HomePage from "./pages/HomePage";
import RecipesPage from "./pages/RecipesPage";
import SubmitRecipePage from "./pages/SubmitRecipePage";
import RecipeDetailPage from "./pages/RecipeDetailPage";

const App = () => {
	return <Router>
			<Route path="/" exact component={HomePage} />
			<Route path="/recipes" component={RecipesPage} />
			<Route path="/submit-recipe" component={SubmitRecipePage} />
			<Route path="/recipe/:recipeSlug" component={RecipeDetailPage} />
		</Router>
}

export default App;
