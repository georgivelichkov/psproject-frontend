import React from 'react';

const Header = () => {
	return <>
		<nav className="navbar navbar-expand-lg navbar-light fixed-top">
			<div className="container-fluid justify-content-center">
				<a className="navbar-brand" href="/"><i className="fa fa-cutlery" aria-hidden="true"/> Recipe E-Book</a>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
						aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
					<span className="navbar-toggler-icon"/>
				</button>
				<div className="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
					<ul className="navbar-nav">
						<li className="nav-item">
							<a className="nav-link" href="/">Home</a>
						</li>
						<li className="nav-item">
							<a className="nav-link" href="/recipes">Recipes Page</a>
						</li>

						<li className="nav-item btn-submit-recipe">
							<a className="nav-link" href="/submit-recipe"><i className="fa fa-upload"
																				  aria-hidden="true"/> Submit Recipe</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		</>
}

export default Header;
