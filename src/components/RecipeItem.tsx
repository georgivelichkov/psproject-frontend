import React from "react";

const RecipeItem = () => {
	return(
		<div className="col-lg-4 col-sm-6">
			<div className="box grid recipes">
				<div className="by"><i className="fa fa-user" aria-hidden="true"/> Gerina Amy</div>
				<a href="/recipe/test"><img src="images/recipe2.jpg" alt=""/></a>
				<h2><a href="/recipe/test">Milk fruit fresh with vegetables </a></h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				<div className="tag">
					<a href="#">Milk</a>
					<a href="#">Lemon</a>
					<a href="#">Sayur</a>
				</div>
			</div>
		</div>
	)
}

export default RecipeItem;
