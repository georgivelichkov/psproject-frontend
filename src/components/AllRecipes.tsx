import React from "react";
import axiosInstance from "../helpers/axios";
import RecipeItem from "./RecipeItem";
import {IRecipe} from "../types/Recipe";

const AllRecipes = () => {

	return <>
		<div className="list">
			<div className="container">
				<div className="row">
					<div className="col-lg-12">
						<h5><i className="fa fa-cutlery" aria-hidden="true"/> List Recipes</h5>
					</div>

					<RecipeItem/>

				</div>
			</div>
		</div>
		</>
}

export default AllRecipes;
